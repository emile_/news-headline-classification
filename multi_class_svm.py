import pickle
import pandas as pd
import numpy as np
import time
import random
import matplotlib.pyplot as plt

from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC

with open("labels_subset.txt", 'r') as handle:
    Y = [x for x in handle]

with open("multi_class_features.pickle", 'rb') as handle:
    Z = pickle.load(handle)

X = []
for i in Z:
    X.append(i.tolist())


labels = ["CRIME", "WORLD NEWS", "WELLNESS", "POLITICS", "TRAVEL", "BUSINESS", "SPORTS"]

def test_svm(w, x_test, y_test):
    fails = 0
    for i,val in enumerate(x_test):
        pred = 1 if np.dot(w, val) > 0 else -1
        if y_test[i] != pred:
            fails+=1
    print("Absolute # fails: " +str(fails))
    print("Fail percentage: "+str(fails/len(y_test)))
    print("Accuracy: "+str((1-fails/len(y_test))*100))

def fit_svm(t_x, t_y):
    w = np.zeros(len(t_x[0]))
    tolerance = 0.01
    minimum_steps = 10000
    lr = 0.001
    order = np.arange(0, len(t_x), 1)
    margin_curr = 0

    converged = False
    t = 0
    #start_time = time.time()
    while not converged:
        margin_prev = margin_curr
        t += 1
        pos_support_vecs = 0
        neg_support_vecs = 0

        eta = 1/(lr*t)
        fac = (1-(eta*lr))*w
        random.shuffle(order)
        for i in order:
            prediction = np.dot(t_x[i], w)
            if (round((prediction),1) == 1):
                pos_support_vecs+=1
            if (round((prediction), 1) == -1):
                neg_support_vecs+=1
                
            if (t_y[i]*prediction) < 1: # incorrect predicted class
                w = fac + eta*t_y[i]*t_x[i]
            else: # correct
                w = fac
        if (t > minimum_steps):
            margin_curr = np.linalg.norm(w)
            if ((pos_support_vecs > 0) and (neg_support_vecs > 0) and ((margin_curr - margin_prev) < tolerance)):
                converged = True
    return w

def generate_subset_data(X, Y):
    X_subset = []
    Y_subset = []
    for z, y_label in enumerate(Y):
        if label in y_label:
            Y_subset.append(1)
            X_subset.append(X[z])
        elif label2 in y_label:
            Y_subset.append(-1)
            X_subset.append(X[z])

    x_test = X_subset[:600]
    y_test = Y_subset[:600]
    x_test = np.c_[x_test, np.ones(len(x_test))]
    x = X_subset[600:]
    y = Y_subset[600:]
    x = np.c_[x, np.ones(len(x))]

    return x, y, x_test, y_test


def run_and_test_SVC(x,y, x_test, y_test):
    print("Fitting SVC lib implementation...")
    clf = make_pipeline(StandardScaler(), SVC(gamma="auto"))
    clf.fit(x, y)
    print("Testing SVC ...")
    accuracy = clf.score(x_test, y_test)
    print("Accuracy: "+str(accuracy))

w_matrix = []
for i, label in enumerate(labels): 
    w_row = []
    for j, label2 in enumerate(labels): 
        if j>i:
            print("="*10)
            print(label+" x "+label2)
            x, y, x_test, y_test = generate_subset_data(X, Y)
            print("Fitting implemented svm...")
            t_w = fit_svm(x, y)
            print("Testing")
            test_svm(t_w, x_test, y_test)
            w_row.append(t_w)

            run_and_test_SVC(x, y, x_test, y_test)
            print("-"*20)
        else:
            w_row.append([])
    w_matrix.append(w_row)

with open("big_w_matrix.pickle", 'wb') as handle:
    pickle.dump(w_matrix,handle)

