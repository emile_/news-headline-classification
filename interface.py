import pickle
from transformers import DistilBertTokenizer, TFDistilBertModel
import tensorflow as tf
from svm import MulticlassSVM
tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-uncased')
model = TFDistilBertModel.from_pretrained("distilbert-base-uncased")

with open("svm.pickle", 'rb') as handle:
    svm = pickle.load(handle)

print()
print("="*40)
encoded_input = tokenizer(input("Headline: "), return_tensors='tf')
output = model(encoded_input)
x = output.last_hidden_state[:,0,:].numpy().flatten()
print("Predicted category: "+str(svm.predict(x.tolist()+[1])))
print("="*40)
print()
