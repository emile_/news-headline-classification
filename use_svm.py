from sklearn.datasets import make_blobs
import pickle
import pandas as pd
import numpy as np
import time
import random
import matplotlib.pyplot as plt

from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.metrics import confusion_matrix

with open("labels_subset.txt", 'r') as handle:
    Y = [x for x in handle]

with open("multi_class_features.pickle", 'rb') as handle:
    X = pickle.load(handle)

with open("big_w_matrix.pickle", 'rb') as handle:
    w_matrix = pickle.load(handle)

for i in range(len(w_matrix)):
    for j in range(len(w_matrix[i])):
        if len(w_matrix[i][j])>0:
            print(1,end=", ")
        else:
            print(0,end=", ")
    print()

labels = ["CRIME", "WORLD NEWS", "WELLNESS", "POLITICS", "TRAVEL", "BUSINESS", "SPORTS"]

test_size =600 
fails = 0
confusion = np.zeros((len(w_matrix), len(w_matrix)))
for u in range(test_size):
    predictions = []
    buckets = np.zeros(len(labels))
    for i in range(len(w_matrix)): 
        for j in range(len(w_matrix[i])): 
            if len(w_matrix[i][j]) > 0:
                p = np.dot(w_matrix[i][j], X[u].tolist()+[1]) 
                index = i if p>0 else j
                buckets[index] += abs(p)
    if labels[np.argmax(buckets)] not in Y[u]:
        fails+=1
        if "WORLD NEWS" in Y[u] and "POLITICS" in labels[np.argmax(buckets)]:
            print(Y[u])
            print(labels[np.argmax(buckets)])
            print(u)
    confusion[labels.index(Y[u][:-1])][np.argmax(buckets)] += 1

print("="*20)
print("Abs fails: "+str(fails))
print("Prec fails: "+str(fails/test_size))
print("Acc: "+str((1-fails/test_size)*100))
print("="*20)

for i in range(len(confusion)):
    print("{:11s} {:5.0f}".format(labels[i], confusion[i].sum()), end=": ")
    for j in range(len(confusion[i])):
        print("{:3.2f}".format(confusion[i][j]/confusion[i].sum()),end=", ")
    print()

# clf = make_pipeline(StandardScaler(), SVC(gamma="auto"))
# clf.fit(X[600:], Y[600:])
# accuracy = clf.score(X[:600], Y[:600])
# print("clf acc")
# print(accuracy)
# predicted = clf.predict(X[:600])
# CM = confusion_matrix(Y[:600], predicted)
# print(CM)

