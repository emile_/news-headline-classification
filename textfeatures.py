import pickle
from transformers import DistilBertTokenizer, TFDistilBertModel
import tensorflow as tf
tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-uncased')
model = TFDistilBertModel.from_pretrained("distilbert-base-uncased")

with open("features.pickle", 'wb') as handle:
    features = []
    with open("crime_and_worldnews_headlines.txt", 'r') as rfile:
        for line in rfile:
            encoded_input = tokenizer(line, return_tensors='tf')
            output = model(encoded_input)
            features.append(output.last_hidden_state[:,0,:].numpy().flatten())
    pickle.dump(features, handle)

