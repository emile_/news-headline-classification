from sklearn.datasets import make_blobs
import pickle
import pandas as pd
import numpy as np
import time
import random
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from copy import deepcopy
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score

with open("labels_subset.txt", 'r') as handle:
    Y = [x for x in handle]

with open("multi_class_features.pickle", 'rb') as handle:
    X = pickle.load(handle)

# Create a dataframe containing the features, cluster, cluter distance (score) and true labels of the headlines
point_dict= {"features":X, "cluster":[0 for x in range(len(X))], "score":[0 for x in range(len(X))], "true_label":Y}
points = pd.DataFrame(point_dict)

labels = ["CRIME", "WORLD NEWS", "WELLNESS", "POLITICS", "TRAVEL", "BUSINESS", "SPORTS"]

# Calculate the cluster for each datapoint based on the smallest distance centroid, and calculate general cost of clustering
def calculate_clusters(points, centroids):
    cost = 0
    for index in points.index:
        j = 0
        dist = -100
        for q, c in np.ndenumerate(centroids):
            # Distance is calculated using the norm of the difference between point and centroid features
            t_dist = np.linalg.norm(points.at[index, "features"] - c)
            if dist == -100 or t_dist < dist:
                j = q
                dist = t_dist
        cost += dist
        # Update datapoint with new cluster and distance score
        points.at[index,"cluster"] = j[0]
        points.at[index,"score"] = dist
    return cost

# Generate new centroids based on the mean of the features of all points in a cluster
def generate_centroids(points, K):
    centroids=np.zeros(shape=(K, len(points.iloc[0]["features"])))
    for i in range(K):
        centroids[i] = points[points["cluster"]==i]["features"].mean()
    return centroids

# Plot points colored by cluster and centroids
def plot_clusters(points, centroids):
    pca = PCA(n_components=2)
    res = pca.fit_transform(X)
    pca_df = pd.DataFrame(data = res, columns=["PC1", "PC2"])
    fig = plt.figure(figsize = (8,8))
    ax = fig.add_subplot(1,1,1) 
    colors = ['r', 'g', 'b','c','m','y','k']
    for i in range(len(centroids)):
        indicesToKeep = points['cluster'] == i
        ax.scatter(pca_df.loc[indicesToKeep, 'PC1']
           , pca_df.loc[indicesToKeep, 'PC2'], c = colors[i], s = 50)

    c_df = pd.DataFrame(data= pca.transform(centroids), columns=["PC1","PC2"])
    ax.scatter(c_df["PC1"], c_df["PC2"], marker='x', c='k', s=200)
    plt.show()

# Main function, runs the clustering algorithm until convergence
def run_kmeans(points, K):
    centroids = points.sample(n=K)["features"].values
    converged = False
    steps = 0
    # For the sake of performance we stop after 5 steps
    while not converged and steps < 5:
        # Deepcopy of the old centroids to make sure the variables don't point to the same list
        old_centroids = deepcopy(centroids)
        cost = calculate_clusters(points, centroids)
        centroids = generate_centroids(points, K)
        # Calculate and print current silhouette score
        print("silhouette: "+str(silhouette_score(points["features"].values.tolist(), points["cluster"].values.tolist())))
        # Optionally plot the clusters at this point
        # plot_clusters(points, centroids)
        if steps>0:
            # Check if the centroids changed since last step, if not convergence is reached
            converged = np.allclose(old_centroids,  centroids)
        steps+=1
    return points, centroids

# Calculate the jaccard index for 2 lists
def jaccard_similarity(list1, list2):
    s1 = set(list1)
    s2 = set(list2)
    return len(s1.intersection(s2)) / len(s1.union(s2))

# Try to find cluster-label couples using the jaccard index
def calculate_cluster_label_couples(points, labels):
    for i in range(k):
        A = points.index[points["cluster"]==i].tolist()
        jbuckets = np.zeros(len(labels))
        # Calculate jaccard similarity with each label and choose the one with the highest score
        for j, label in enumerate(labels):
            B = points.index[points['true_label'] == label+"\n"].tolist() 
            jbuckets[j] = jaccard_similarity(A, B)
        print("cluster "+str(i)+": "+labels[np.argmax(jbuckets)])

points, centroids = run_kmeans(points, k)
calculate_cluster_label_couples(points, labels)


