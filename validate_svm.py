import pickle
import pandas as pd
import numpy as np
import time
import random
import matplotlib.pyplot as plt
from svm import MulticlassSVM

with open("labels_subset.txt", 'r') as handle:
    Y = [x for x in handle]

with open("multi_class_features.pickle", 'rb') as handle:
    X = pickle.load(handle)

with open("svm.pickle", 'rb') as handle:
    svm = pickle.load(handle)

for u in range(10):
    print(svm.predict(X[u].tolist()+[1]))
