import json
import random
with open("News_Category_Dataset_v2.json") as file:
    with open("labels_subset.txt", 'w') as l_file:
        with open("headlines_subset.txt", 'w') as h_file:
            data = json.load(file)
            random.shuffle(data["headlines"])
            for line in data["headlines"]:
                if line["category"] in ["CRIME", "WORLD NEWS", "WELLNESS", "POLITICS", "TRAVEL", "BUSINESS", "SPORTS"]:
                    l_file.write(line["category"]+"\n")
                    h_file.write(line["headline"]+"\n")
        
