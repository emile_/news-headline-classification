from sklearn.datasets import make_blobs
import pickle
import pandas as pd
import numpy as np
import time
import random
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from copy import deepcopy
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score

with open("labels_subset.txt", 'r') as handle:
#with open("crime_and_worldnews_labels.txt", 'r') as handle:
    Y = [x for x in handle]


with open("multi_class_features.pickle", 'rb') as handle:
    X = pickle.load(handle)

labels = ["CRIME", "WORLD NEWS", "WELLNESS", "POLITICS", "TRAVEL", "BUSINESS", "SPORTS"]


def plot_clusters(points, centroids, l):
    pca = PCA(n_components=2)
    res = pca.fit_transform(X)
    pca_df = pd.DataFrame(data = res, columns=["PC1", "PC2"])
    fig = plt.figure(figsize = (8,8))
    ax = fig.add_subplot(1,1,1) 
    colors = ['r', 'g', 'b','c','m','y','k']
    for i in range(len(centroids)):
        indicesToKeep = points['cluster'] == i
        ax.scatter(pca_df.loc[indicesToKeep, 'PC1']
           , pca_df.loc[indicesToKeep, 'PC2'], c = colors[i], s = 10,alpha=0.3)
    c_df = pd.DataFrame(data= pca.transform(centroids), columns=["PC1","PC2"])
    ax.scatter(c_df["PC1"], c_df["PC2"], marker='x', c='r', s=250)
    ax.legend(l)
    plt.show()


def jaccard_similarity(list1, list2):
    s1 = set(list1)
    s2 = set(list2)
    return len(s1.intersection(s2)) / len(s1.union(s2))
    
kmeans = KMeans(n_clusters=len(labels), random_state=0).fit(X)
print(silhouette_score(X, kmeans.labels_, metric='euclidean'))
point_dict= {"features":X, "cluster":kmeans.labels_, "true_label":Y}
points = pd.DataFrame(point_dict)
calc_labels = []
for i in range(len(labels)):
    A = points.index[points["cluster"]==i].tolist()
    jbuckets = np.zeros(len(labels))
    for j, label in enumerate(labels):
        B = points.index[points['true_label'] == label+"\n"].tolist()
        jbuckets[j] = jaccard_similarity(A, B)
    print("cluster "+str(i)+": "+labels[np.argmax(jbuckets)])
    calc_labels.append(labels[np.argmax(jbuckets)])

plot_clusters(points, kmeans.cluster_centers_, calc_labels)
