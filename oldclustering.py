from sklearn.datasets import make_blobs
import pickle
import pandas as pd
import numpy as np
import time
import random
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from copy import deepcopy

with open("crime_and_worldnews_labels.txt", 'r') as handle:
    Y = [x for x in handle]
    #Y = [-1 if "CRIME" in x else 1 for x in handle]

with open("features.pickle", 'rb') as handle:
    X = pickle.load(handle)

#X = []
#for i in Z:
#    X.append([round(z, 5) for z in i.tolist()][:767])


point_dict= {"features":X, "cluster":[0 for x in range(len(X))], "score":[0.0 for x in range(len(X))]}
points = pd.DataFrame(point_dict)

labels = ["CRIME", "WORLD_NEWS"]

def calculate_clusters(points, centroids):
    cost = 0
    for index, row in points.iterrows():
        j = 0
        dist = -100
        for q, c in np.ndenumerate(centroids):
            t_dist = np.linalg.norm(row["features"] - c)
            if dist == -100 or t_dist < dist:
                j = q
                dist = t_dist
        cost += dist
        points.loc[index, "cluster"] = j[0]
        points.loc[index, "score"] = dist
    return cost, points

def generate_centroids(points, K):
    centroids = []
    for i in range(K):
        centroids.append(np.mean(points.loc[points["cluster"]==i]["features"]))
    return centroids
    

def run_kmeans(points, K):
    centroids = points.sample(n=K)["features"].values
    print(centroids.shape)
    converged = False
    t = 0
    #while not converged:
    while t<1:
        old_centroids = deepcopy(centroids)
        cost, points = calculate_clusters(points, centroids)
        centroids = generate_centroids(points, K)
        t+=1
        print(t)
        #converged = np.array_equal(old_centroids, centroids)
    return points, centroids


points, centroids = run_kmeans(points, 2)

np.random.seed(42)
rndperm = np.random.permutation(points.shape[0])

pca = PCA(n_components=3)
print(points.shape)
print(points["features"].shape)
#pca_result = pca.fit_transform(points["features"])
pca_result = pca.fit_transform(np.array(X))
points['pca-one'] = pca_result[:,0]
points['pca-two'] = pca_result[:,1]
points['pca-three'] = pca_result[:,2]

ax = plt.figure(figsize=(16,10)).gca(projection='3d')
ax.scatter(
    xs=points.loc[rndperm,:]["pca-one"], 
    ys=points.loc[rndperm,:]["pca-two"], 
    zs=points.loc[rndperm,:]["pca-three"], 
    c=points.loc[rndperm,:]["cluster"], 
    cmap='tab10'
)
ax.set_xlabel('pca-one')
ax.set_ylabel('pca-two')
ax.set_zlabel('pca-three')
plt.show()


    
