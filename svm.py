import pickle
import numpy as np
import time
import random

# Binary SVM class as the basis of predictions
class BinarySVM():
    def __init__(self, labels, lr, tol, min_steps):
        self.labels = labels
        self.lr = lr
        self.tolerance = tol
        self.min_steps = min_steps
        self.acc = 0
        self.w = [] 

    # Compares predictions to actual targets and prints out accuracy score
    def validate(self, x_test, y_test):
        fails = 0
        for i,val in enumerate(x_test):
            pred = 1 if np.dot(self.w, val) > 0 else -1
            if y_test[i] != pred:
                fails+=1
        self.acc = (1-fails/len(y_test))*100
        print("Absolute # fails: " +str(fails))
        print("Fail percentage: "+str(fails/len(y_test)))
        print("Accuracy: "+str(self.acc))

    def fit(self, X, Y):
        # Initialise empty vector w, 
        self.w = np.zeros(len(X[0]))
        order = np.arange(0, len(X), 1)
        margin_curr = 0
        converged = False
        t = 0
        while not converged:
            margin_prev = margin_curr
            t += 1
            pos_support_vecs = 0
            neg_support_vecs = 0

            # Variables for updating w
            eta = 1/(self.lr*t)
            fac = (1-(eta*self.lr))*self.w
            random.shuffle(order)
            for i in order:
                prediction = np.dot(X[i], self.w)
                # Count support vectors for each class
                if round((prediction), 1) == 1:
                    pos_support_vecs+=1
                if round((prediction), 1) == -1:
                    neg_support_vecs+=1
                    
                if Y[i]*prediction < 1: # Incorrect predicted class
                    self.w = fac + eta*Y[i]*X[i]
                else: # Correct class
                    self.w = fac
            if (t > self.min_steps):
                margin_curr = np.linalg.norm(self.w)
                # Make sure there are support vectors for each class and the margin difference between prev step is small enough
                if pos_support_vecs > 0 and neg_support_vecs > 0 and margin_curr - margin_prev < self.tolerance:
                    converged = True

    def predict(self, sample):
        x = sample.tolist()
        if len(x) < len(self.w):
            x = x+[1]
        p = np.dot(self.w, x)
        index = 0 if p > 0 else 1
        return self.labels[index], p

# Wrapper for binary class SVMs to use in a multi class context
class MulticlassSVM():
    def __init__(self, labels, lr):
        self.labels = labels
        self.lr = lr
        self.tolerance = 0.01
        self.min_steps = 10000
        self.binary_svms = []
        self.acc = 0
        self.confusion = np.zeros((len(labels), len(labels)))

    def test(self, x_test, y_test):
        fails = 0
        tested_samples = 0
        for i in range(len(x_test)):
            if y_test[i][:-1] in self.labels:
                tested_samples += 1
                prediction = self.predict(x_test[i])
                # Update confusion matrix
                self.confusion[self.labels.index(y_test[i][:-1])][self.labels.index(prediction)] += 1
                if prediction not in y_test[i]:
                    fails += 1
        self.acc = (1-fails/tested_samples)*100
        print("Tested samples: " +str(tested_samples))
        print("Absolute # fails: " +str(fails))
        print("Fail percentage: "+str(fails/tested_samples))
        print("Accuracy: "+str(self.acc))
            
    def print_confusion(self):
        for i in range(len(self.confusion)):
            print("{:11s} {:5.0f}".format(self.labels[i], self.confusion[i].sum()), end=": ")
            for j in range(len(self.confusion[i])):
                print("{:3.2f}".format(self.confusion[i][j]/self.confusion[i].sum()),end=", ")
            print()

    # Extract only data for given labels from multi class data and convert labels to 1 and -1
    def generate_binary_data(self, label1, label2, X, Y):
        X_subset = []
        Y_subset = []
        for z, y_label in enumerate(Y):
            if label1 in y_label:
                Y_subset.append(1)
                X_subset.append(X[z])
            elif label2 in y_label:
                Y_subset.append(-1)
                X_subset.append(X[z])
        x_test = X_subset[:500]
        y_test = Y_subset[:500]
        x_test = np.c_[x_test, np.ones(len(x_test))]

        # Equal class split of training data 
        x = X_subset[500:min(Y_subset.count(1), Y_subset.count(-1))] 
        y = Y_subset[500:min(Y_subset.count(1), Y_subset.count(-1))]
        x = np.c_[x, np.ones(len(x))]

        return x, y, x_test, y_test

    def fit(self, X, Y):
        for i, label1 in enumerate(self.labels): 
            for j, label2 in enumerate(self.labels): 
                # Only 1 binary svm for each combination is necessary, and not for the same class
                if j>i:
                    print("="*10)
                    print(label1+" x "+label2)
                    x, y, x_test, y_test = self.generate_binary_data(label1, label2, X, Y)
                    b_svm = BinarySVM([label1, label2], self.lr, self.tolerance, self.min_steps)
                    b_svm.fit(x, y)
                    b_svm.validate(x_test, y_test)
                    self.binary_svms.append(b_svm)
                    print("-"*20)

    def predict(self, sample):
        buckets = np.zeros(len(self.labels))
        for i in range(len(self.binary_svms)):
            p_label, p = self.binary_svms[i].predict(sample)
            buckets[self.labels.index(p_label)] += abs(p)
        return self.labels[np.argmax(buckets)]


