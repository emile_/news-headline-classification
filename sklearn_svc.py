import pickle
import pandas as pd
import numpy as np

from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC

with open("labels_subset.txt", 'r') as handle:
    Y = [x for x in handle]

with open("multi_class_features.pickle", 'rb') as handle:
    Z = pickle.load(handle)

X = []
for i in Z:
    X.append(i.tolist())

labels = ["CRIME", "WORLD NEWS", "WELLNESS", "POLITICS", "TRAVEL", "BUSINESS", "SPORTS"]

def generate_subset_data(label1, label2, X, Y):
    X_subset = []
    Y_subset = []
    for z, y_label in enumerate(Y):
        if label1 in y_label:
            Y_subset.append(1)
            X_subset.append(X[z])
        elif label2 in y_label:
            Y_subset.append(-1)
            X_subset.append(X[z])

    x_test = X_subset[:500]
    y_test = Y_subset[:500]
    x_test = np.c_[x_test, np.ones(len(x_test))]
    # equal class split of training data 
    x = X_subset[500:min(Y_subset.count(1), Y_subset.count(-1))] 
    y = Y_subset[500:min(Y_subset.count(1), Y_subset.count(-1))]
    x = np.c_[x, np.ones(len(x))]

    return x, y, x_test, y_test


def run_and_test_SVC(x,y, x_test, y_test):
    print("Fitting SVC lib implementation...")
    clf = make_pipeline(StandardScaler(), SVC(gamma="auto"))
    clf.fit(x, y)
    print("Testing SVC ...")
    accuracy = clf.score(x_test, y_test)
    print("Accuracy: "+str(accuracy))

for i, label1 in enumerate(labels): 
    for j, label2 in enumerate(labels): 
        if j>i:
            print("="*10)
            print(label1+" x "+label2)
            x, y, x_test, y_test = generate_subset_data(label1, label2, X, Y)
            run_and_test_SVC(x, y, x_test, y_test)
            print("-"*20)

