import pickle
import pandas as pd
import numpy as np
import time
import random
from svm import MulticlassSVM

from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC

# Load target labels for training 
with open("labels_subset.txt", 'r') as handle:
    Y = [x for x in handle]

# Load precalculated text features for training
with open("multi_class_features.pickle", 'rb') as handle:
    X = pickle.load(handle)

# Define ordered list of labels to be trained and recognized by the svm
labels = ["CRIME", "WORLD NEWS", "WELLNESS", "POLITICS", "TRAVEL", "BUSINESS", "SPORTS"]

svm = MulticlassSVM(labels, 0.01)
svm.fit(X[200:], Y[200:])
print("^"*20)
print("multi class svm fitted")
svm.test(X[:200], Y[:200])
svm.print_confusion()

with open("svm_small.pickle", 'wb') as handle:
    pickle.dump(svm,handle)

